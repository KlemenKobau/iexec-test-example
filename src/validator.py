import os
import sys
import json

iexec_out = os.environ['IEXEC_OUT']
iexec_in = os.environ['IEXEC_IN']

creditor_name = sys.argv[1]

status = 'ERROR'

# Use some confidential assets
if os.path.exists(iexec_in + '/creditors.json'):
    with open(iexec_in + '/creditors.json', 'r') as f:
        lines = f.readlines()
        stripped = list(map(str.strip, lines))
        joined = "".join(stripped)
        loaded = json.loads(joined)

        creditors = loaded['creditors']

        for creditor in creditors:
            if creditor['name'] == creditor_name:
                if int(creditor['monthlyIncome']) > 1000:
                    status = '{}, your loan is approved'.format(creditor_name)
                else:
                    status = '{}, your loan is denied'.format(creditor_name)

# Append some results
with open(iexec_out + '/result.txt', 'w+') as f:
    f.write(status)

# Declare everything is computed
with open(iexec_out + '/computed.json', 'w+') as f:
    json.dump({ "deterministic-output-path" : iexec_out + '/result.txt' }, f)
