# Iexec project
The setup is based on the official iexec setup page https://docs.iex.ec/for-developers/quick-start-for-developers.

## initial setup

First we will install the ixec CLI.
On linux we can use the alias command to shorten the commands, on windows use *./node_modules/.bin/iexec*
when executing the commands instead.
```
npm install
alias iexec='./node_modules/.bin/iexec'
```

## creating the wallet

The following command creates a wallet on the computer.
```
iexec wallet create
```

Use *iexec init --skip-wallet* when creating a new project, but this is unnecessary as this
repo already contains all required files

Checking the wallet
```
iexec wallet show --chain goerli
```

If you are starting from scratch then your wallet will be empty.
You can get some test ether by following the guide on https://faucet.goerli.mudit.blog/.

Initialize the remote storage
```
iexec storage init --chain goerli
```

Now run
```
iexec app init
```
and have a look at the file *iexec.json*

Under the app part we can find the config for our application
```json
{
  ...
  "app": {
    "owner": "<owner wallet address, default the creator's>", 
    "name": "<name of the app>",
    "type": "DOCKER",
    "multiaddr": "<the container address on docker hub, e.g. registry.hub.docker.com/klemenkobau/validator-tee:latest>",
    "checksum": "0x + <container checksum>",
    "mrenclave": "<mrenclave, used in confidential computing, printed when building a scone image>"
  },
  ...
}

```

Push the application to the docker hub and copy the checksum into *ixec.json*.
Now we can run the application:
- use *--tag tee* when using confidential computing
- use *--args run_on_tee* to set the arguments
- use *--encrypt-result* when you wish to encrypt the results (requires some setup mentioned later)
```
iexec app run --args run_on_tee --workerpool iexec-main-pool.workerpools.v5.iexec.eth --watch --chain goerli --tag tee --encrypt-result
```

Download the result
```
iexec task show <0x-your-task-id> --download --chain goerli
```

If you encrypted the result decrypt it with
```
iexec result decrypt <0x-your-task-id.zip>
```

Your results should be contained within the results.zip!


## Dataset
Ixec supports the use of datasets

init the dataset with
```
iexec dataset init --encrypted
```
put your dataset under
*./datasets/original/dataset_name*
and encrypt it using
```
iexec dataset encrypt --algorithm scone
```

Similarly to the deployment of the app, we have to configure *ixec.json* this time the dataset part.
```json
{
  ...

  "dataset": {
    "owner": "<owner wallet address, default the creator's>",
    "name": "<name of the dataset>",
    "multiaddr": "<where to download the dataset from, must be retrievable with only a GET request>",
    "checksum": "<dataset's checksum>"
  }
}
```

Next we can deploy the dataset using
```
iexec dataset deploy --chain goerli
```

and finally push the secret by using
```
iexec dataset push-secret <0x-your-dataset-address> --chain goerli
```

## Encrypting the result
First, we have to generate the key pair and push them to the chaincode
```
iexec result generate-encryption-keypair
iexec result push-encryption-key --chain goerli
```

We run the app similarly as before, we only use the *--tag tee* and *--encrypt-result* arguments to encrypt.
We then decrypt the result as described above.

